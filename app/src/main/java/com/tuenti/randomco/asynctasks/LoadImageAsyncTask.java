package com.tuenti.randomco.asynctasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.tuenti.randomco.R;
import com.tuenti.randomco.controller.Controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Rako on 08/05/2017.
 */
public class LoadImageAsyncTask extends AsyncTask<Void, Void, Bitmap> {

    ImageView imageView;
    String url="";
    String username;

    public LoadImageAsyncTask(ImageView imageView, String url, String username){
        this.imageView=imageView;
        this.url=url;
        this.username=username;
    }

    @Override
    protected void onPreExecute() {}

    @Override
    protected Bitmap doInBackground(Void... params) {
        Bitmap bitmap=null;
        URL imageURL;
        try {
            imageURL = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) imageURL.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            bitmap = BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        imageView.setImageBitmap(result);
        if(result!=null){
            imageView.setImageBitmap(result);
            //Controller.getInstance().getUsersList().get(position).setImgUser(result);
            Controller.getInstance().getUsersHashMap().get(username).setImgUser(result);
        }else{
            imageView.setImageResource(R.drawable.user_default_image);
        }
    }

}
