package com.tuenti.randomco.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tuenti.randomco.R;
import com.tuenti.randomco.asynctasks.LoadImageAsyncTask;
import com.tuenti.randomco.beans.user.User;
import com.tuenti.randomco.utils.UtilsNetwork;

import java.util.ArrayList;

/**
 * Created by Rako on 07/05/2017.
 */
public class UserPagerAdapter extends PagerAdapter {

    private Activity mActivity;
    private LayoutInflater mLayoutInflater;
    private ArrayList<User> userList;
    //private boolean isPortrait;

    public UserPagerAdapter(Activity activity, ArrayList<User> userList) {
        mActivity = activity;
        mLayoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.userList=userList;
        //isPortrait= Utils.getScreenOrientation(mActivity)== Configuration.ORIENTATION_PORTRAIT;
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.page_user, container, false);

        TextView tvUserName = (TextView) itemView.findViewById(R.id.tvPageUserName);
        tvUserName.setText(userList.get(position).getName().toString());

        TextView tvPagerUserGender = (TextView) itemView.findViewById(R.id.tvPagerUserGender);
        tvPagerUserGender.setText(userList.get(position).getGender());

        TextView tvUserAddress = (TextView) itemView.findViewById(R.id.tvPagerUserAddress);
        tvUserAddress.setText(userList.get(position).getLocation().toString());

        TextView tvPagerUserEmail = (TextView) itemView.findViewById(R.id.tvPagerUserEmail);
        tvPagerUserEmail.setText(userList.get(position).getEmail());

        TextView tvPagerUserRegisteredDate = (TextView) itemView.findViewById(R.id.tvPagerUserRegisteredDate);
        tvPagerUserRegisteredDate.setText(userList.get(position).getRegistered());

        ImageView imgUser = (ImageView) itemView.findViewById(R.id.imgUser);
        //imgUser.setScaleType(isPortrait?ImageView.ScaleType.CENTER_CROP:ImageView.ScaleType.CENTER_INSIDE);
        if(userList.get(position).getImgUser()!=null){
            imgUser.setImageBitmap(userList.get(position).getImgUser());
        }else{
            imgUser.setImageResource(R.drawable.user_default_image);
            if(UtilsNetwork.isDeviceOnline(mActivity) && URLUtil.isValidUrl(userList.get(position).getPicture().getLarge())){
                new LoadImageAsyncTask(imgUser,userList.get(position).getPicture().getLarge(), userList.get(position).getLogin().getUsername()).execute();
            }else{
                imgUser.setImageResource(R.drawable.user_default_image);
            }
        }
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}