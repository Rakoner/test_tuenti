package com.tuenti.randomco.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tuenti.randomco.R;
import com.tuenti.randomco.activities.UserDetailActivity;
import com.tuenti.randomco.asynctasks.LoadImageAsyncTask;
import com.tuenti.randomco.beans.user.User;
import com.tuenti.randomco.controller.Controller;
import com.tuenti.randomco.services.UserService;
import com.tuenti.randomco.utils.ExtraParams;

import java.util.ArrayList;

/**
 * Created by Rako on 07/05/2017.
 */
public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UserViewHolder>  implements Filterable {

    private ArrayList<User> usersList;
    private ArrayList<User> displayedUsersList;
    private Context mContext;

    public UsersAdapter(ArrayList<User> usersList, Context aCtx) {
        this.usersList = usersList;
        this.displayedUsersList=usersList;
        if(usersList!=null){
            this.usersList = usersList;
            this.displayedUsersList = usersList;
        }else{
            this.usersList = new ArrayList<>();
            this.displayedUsersList = new ArrayList<>();
        }
        this.mContext=aCtx;
    }

    public ArrayList<User> getDisplayedUsersList() {
        return displayedUsersList;
    }

    public void updateUsers() {
        usersList = UserService.getUsersOrderedByLinkedList(Controller.getInstance().getUsersListOrderByUserName(),false);
    }


    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user, parent, false);
        return new UserViewHolder(itemView,mContext);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        //User item = Controller.getInstance().getUsersHashMap().get(Controller.getInstance().getUsersListOrderByUserName().get(position));
        User item = displayedUsersList.get(position);
        holder.bindUser(item,position,this);
    }

    @Override
    public int getItemCount() {
        return displayedUsersList.size();
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {

        private com.tuenti.randomco.customviews.ImageViewRoundedFitWidth imgUser;
        private TextView tvUserName;
        private TextView tvUserEmail;
        private TextView tvUserPhone;
        private ImageView imgFavourite;
        private ImageView imgDeleteUser;
        private LinearLayout lnLytItemBackground;
        private Context context;

        public UserViewHolder(View itemView, Context aContext) {
            super(itemView);
            tvUserName = (TextView)itemView.findViewById(R.id.tvUserName);
            tvUserEmail = (TextView)itemView.findViewById(R.id.tvUserEmail);
            tvUserPhone = (TextView)itemView.findViewById(R.id.tvUserPhone);
            imgFavourite = (ImageView)itemView.findViewById(R.id.imgFavourite);
            imgDeleteUser = (ImageView)itemView.findViewById(R.id.imgDeleteUser);
            lnLytItemBackground =  (LinearLayout)itemView.findViewById(R.id.lnLytItemBackground);
            imgUser = (com.tuenti.randomco.customviews.ImageViewRoundedFitWidth)itemView.findViewById(R.id.imgUser);
            context=aContext;
        }

        public void bindUser(User aUser,int aPosition, UsersAdapter userAdapter) {
            tvUserName.setText(aUser.getName().toString());
            tvUserEmail.setText(aUser.getEmail());
            tvUserPhone.setText(aUser.getPhone());
            if(Controller.getInstance().getUsersFavouritesSet().contains(aUser.getLogin().getUsername())){
                imgFavourite.setImageDrawable(context.getResources().getDrawable(R.drawable.favourite_yes));
                lnLytItemBackground.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.item_favourite_background));
            }else{
                imgFavourite.setImageDrawable(context.getResources().getDrawable(R.drawable.favourite_no));
                lnLytItemBackground.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.item_background));
            }
            final String usernameFinal=aUser.getLogin().getUsername();
            final UsersAdapter finalUsersAdapter=userAdapter;
            final int finalPosition=aPosition;
            final Context finalContext=context;
            imgFavourite.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v) {
                    if(Controller.getInstance().getUsersFavouritesSet().contains(usernameFinal)){
                        new UserService().deleteFavouriteUser(usernameFinal);
                    }else{
                        new UserService().addFavouriteUser(usernameFinal);
                    }
                    finalUsersAdapter.notifyDataSetChanged();
                }
            });

            imgDeleteUser.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v) {
                    new UserService().deleteUser(usernameFinal);
                    finalUsersAdapter.getDisplayedUsersList().remove(finalPosition);
                    finalUsersAdapter.updateUsers();
                    finalUsersAdapter.notifyDataSetChanged();
                }
            });

            imgUser.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v) {
                    Intent intent = new Intent(finalContext, UserDetailActivity.class);
                    intent.putExtra(ExtraParams.EXTRA_USER_USERNAME,usernameFinal);
                    finalContext.startActivity(intent);
                }
            });


            //Image Universal Loader Option
            //ImageUtils.loadImage(aCtx, NetworkConstants.USER_IMAGE_URL,imgUser, Constants.DEFAULT_DISPLAY_IMAGE_OPTIONS);

            //Picasso Option
//            OkHttpClient okHttpClient = new OkHttpClient();
//            okHttpClient.interceptors().add(new PicassoInterceptor());
//            OkHttpDownloader downloader = new OkHttpDownloader(okHttpClient);
//            new OkHttpDownloader(okHttpClient);

            //Asynktask Option
            if(Controller.getInstance().getUsersHashMap().get(aUser.getLogin().getUsername()).getImgUser()!=null){
                imgUser.setImageBitmap(aUser.getImgUser());
            }else{
                imgUser.setImageResource(R.drawable.user_default_image);
                if(URLUtil.isValidUrl(Controller.getInstance().getUsersHashMap().get(aUser.getLogin().getUsername()).getPicture().getLarge())){
                    new LoadImageAsyncTask(imgUser,Controller.getInstance().getUsersHashMap().get(aUser.getLogin().getUsername()).getPicture().getLarge(), aUser.getLogin().getUsername()).execute();
                }else{
                    imgUser.setImageResource(R.drawable.user_default_image);
                }
            }

        }
    }



//    private static class PicassoInterceptor implements Interceptor {
//
//        @Override
//        public Response intercept(Chain chain) throws IOException {
//
//            final MediaType JSON
//                    = MediaType.parse("application/json; charset=utf-8");
//            Map<String, String> map = new HashMap<String, String>();
////        map.put("session_id", session_id);
////        map.put("image", image);
//            String requestJsonBody = new Gson().toJson(map);
//            RequestBody body = RequestBody.create(JSON, requestJsonBody);
//            final Request original = chain.request();
//            final Request.Builder requestBuilder = original.newBuilder()
//                    .url(NetworkConstants.USER_IMAGE_URL)
//                    .post(body);
//            return chain.proceed(requestBuilder.build());
//        }
//    }


    private Filter filterUserData;

    @Override
    public Filter getFilter() {
        if(filterUserData == null) {
            filterUserData=new UserDataFilter();
        }
        return filterUserData;
    }


    private class UserDataFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            //usersList = UserService.getUsersOrderedByLinkedList(Controller.getInstance().getUsersListOrderByUserName());
            if (constraint == null || constraint.length() == 0) {
                synchronized (this) {
                    results.values = usersList;
                    results.count = usersList.size();
                }
            } else {
                //Need Filter
                ArrayList<User> usersFiltered = new ArrayList<User>();
                ArrayList<User> usersAll = new ArrayList<User>();

                synchronized (this) {
                    usersAll.addAll(usersList);
                }
                for(User user: usersAll){
                    String userName = user.getName().getFirst();
                    String userSurname = user.getName().getLast();
                    String userEmail = user.getEmail();
                    if (userName!=null && userName.toLowerCase().contains(constraint.toString()) ||
                            userSurname!=null && userSurname.toLowerCase().contains(constraint.toString()) ||
                            userEmail!=null && userEmail.toLowerCase().contains(constraint.toString())) {
                        usersFiltered.add(user);
                    }
                }
                results.count = usersFiltered.size();
                results.values = usersFiltered;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,FilterResults results) {
            displayedUsersList = (ArrayList<User>) results.values;
            notifyDataSetChanged();
        }
    }

}
