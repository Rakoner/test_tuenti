package com.tuenti.randomco.customviews;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

public class ImageViewRoundedFitWidth extends ImageView {

	public int myRing = 5;

	public int getRing() {
		return myRing;
	}

	public void setRing(int aRing) {
		this.myRing = aRing;
	}

	public ImageViewRoundedFitWidth(Context context) {
		super(context);
	}

	public ImageViewRoundedFitWidth(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ImageViewRoundedFitWidth(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width=0;
		int height=0;
		if(getDrawable()!= null){
//			width = MeasureSpec.getSize(widthMeasureSpec);
//			height = width * getDrawable().getIntrinsicHeight() / getDrawable().getIntrinsicWidth();
//			//height = width;
			height = MeasureSpec.getSize(heightMeasureSpec);
			width=height;
			//width = height * getDrawable().getIntrinsicWidth() / getDrawable().getIntrinsicHeight();
			//height = width;
		}
		setMeasuredDimension(width, height);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		if (getDrawable() instanceof BitmapDrawable) {
			BitmapDrawable drawable = (BitmapDrawable) getDrawable();

			if (drawable == null) {
				return;
			}

			if (getWidth() == 0 || getHeight() == 0) {
				return;
			}

			Bitmap fullSizeBitmap = drawable.getBitmap();

			int scaledWidth = getMeasuredWidth();
			int scaledHeight = getMeasuredHeight();

			Bitmap mScaledBitmap=null;
			try {
				if (scaledWidth == fullSizeBitmap.getWidth() && scaledHeight == fullSizeBitmap.getHeight()) {
					mScaledBitmap = fullSizeBitmap;
				} else {
					mScaledBitmap = Bitmap.createScaledBitmap(fullSizeBitmap, scaledWidth, scaledHeight, true /* filter */);
				}
			}catch(Exception e){
				Log.v("EXCEPTION", "Exception ", e);
			}

			// Bitmap roundBitmap = getRoundedCornerBitmap(mScaledBitmap);

//			 Bitmap roundBitmap = getRoundedCornerBitmap(getContext(),
//			 mScaledBitmap, 10, scaledWidth, scaledHeight, false, false,
//			 false, false);
//			 canvas.drawBitmap(roundBitmap, 0, 0, null);

			if (mScaledBitmap!=null) {
				Bitmap circleBitmap = getCircledBitmap(mScaledBitmap);

				canvas.drawBitmap(circleBitmap, 0, 0, null);
			}
		} else {
			super.onDraw(canvas);
		}

	}

	Bitmap getCircledBitmap(Bitmap bitmap) {

		Bitmap result = null;

		try{
			result = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
			Canvas canvas = new Canvas(result);
			int color = Color.BLUE;
			Paint paint = new Paint();
			Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
			paint.setAntiAlias(true);
			canvas.drawARGB(0, 0, 0, 0);
			paint.setColor(color);
			canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, (bitmap.getHeight() / 2) - myRing, paint);
			paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
			canvas.drawBitmap(bitmap, rect, rect, paint);
		}catch(Exception e){
			Log.e("EXCEPTION", "Exception: "+e);
		}
		return result;
	}

}