package com.tuenti.randomco.controller;

import android.content.Context;

import com.tuenti.randomco.beans.user.User;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * Created by Rako on 07/05/2017.
 */
public class Controller {

    private static Controller ourInstance;
    private Context myContext;
    private HashMap<String,User> usersHashMap = new HashMap<>();
    private LinkedList<String> usersListOrderByUserName = new LinkedList<String>();
    private LinkedList<String> usersListOrderByGender = new LinkedList<String>();
    private Set<String> usersFavouritesSet = new HashSet<String>();
    private Set<String> usersDeletedSet = new HashSet<String>();

    public static Controller getInstance() {
        if (ourInstance == null) {
            ourInstance = new Controller();
        }
        return ourInstance;
    }

    public HashMap<String, User> getUsersHashMap() {
        return usersHashMap;
    }

    public void setUsersHashMap(HashMap<String, User> usersHashMap) {
        this.usersHashMap = usersHashMap;
    }

    public LinkedList<String> getUsersListOrderByUserName() {
        return usersListOrderByUserName;
    }

    public void setUsersListOrderByUserName(LinkedList<String> usersListOrderByUserName) {
        this.usersListOrderByUserName = usersListOrderByUserName;
    }

    public LinkedList<String> getUsersListOrderByGender() {
        return usersListOrderByGender;
    }

    public void setUsersListOrderByGender(LinkedList<String> usersListOrderByGender) {
        this.usersListOrderByGender = usersListOrderByGender;
    }

    public Set<String> getUsersFavouritesSet() {
        return usersFavouritesSet;
    }

    public void setUsersFavouritesSet(Set<String> usersFavouritesSet) {
        this.usersFavouritesSet = usersFavouritesSet;
    }

    public Set<String> getUsersDeletedSet() {
        return usersDeletedSet;
    }

    public void setUsersDeletedSet(Set<String> usersDeletedSet) {
        this.usersDeletedSet = usersDeletedSet;
    }
}
