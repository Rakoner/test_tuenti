package com.tuenti.randomco.beans.user;

/**
 * Created by Rako on 07/05/2017.
 */
public class UserName {

    private String first;
    private String last;

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    @Override
    public String toString() {
        String result="";
        if(first!=null){
            result+=first+" ";
        }
        if(last!=null){
            result+=last;
        }
        return result;
    }
}
