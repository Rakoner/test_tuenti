package com.tuenti.randomco.beans.user;

/**
 * Created by Rako on 07/05/2017.
 */
public class UserLogin {

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
