package com.tuenti.randomco.beans.user;

/**
 * Created by Rako on 07/05/2017.
 */
public class UserLocation {

    private String street;
    private String city;
    private String state;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        String result="";
        if(street!=null){
            result+=street+", ";
        }
        if(city!=null){
            result+=city+", ";
        }
        if(state!=null){
            result+=state;
        }
        return result;
    }
}
