package com.tuenti.randomco.services;

import android.app.Activity;
import android.util.Log;

import com.tuenti.randomco.R;
import com.tuenti.randomco.activities.MainActivity;
import com.tuenti.randomco.beans.user.User;
import com.tuenti.randomco.controller.Controller;
import com.tuenti.randomco.utils.Utils;
import com.tuenti.randomco.webservices.WebServicesConstants;
import com.tuenti.randomco.webservices.response.GetUsersResponse;
import com.tuenti.randomco.webservices.services.ServiceGenerator;
import com.tuenti.randomco.webservices.services.UserWebService;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rako on 07/05/2017.
 */
public class UserService {

    public void processGetUsersServerCall(GetUsersResponse getUsersResponse){
        insertUsers(getUsersResponse.getResults());
    }

    public void insertUsers(ArrayList<User> aUserlist){
        if(aUserlist != null){
            for(User user: aUserlist){
                addUser(user);
            }
        }
    }

    public void addUser(User aUser){
        if(aUser!=null && aUser.getLogin().getUsername()!=null && !Controller.getInstance().getUsersDeletedSet().contains(aUser.getLogin().getUsername())){
            //we insert into the not sorted hashmap
            Controller.getInstance().getUsersHashMap().put(aUser.getLogin().getUsername(),aUser);
            //we insert into the sorted linkedlist
            insertUserOrdererByName(aUser.getLogin().getUsername(),aUser.getName().getFirst());
        }
    }

    public void deleteUser(String idUser){
        Controller.getInstance().getUsersHashMap().remove(idUser);
        Controller.getInstance().getUsersListOrderByUserName().remove(idUser);
        Controller.getInstance().getUsersFavouritesSet().remove(idUser);
        Controller.getInstance().getUsersDeletedSet().add(idUser);
    }

    public void addFavouriteUser(String idUser){
        Controller.getInstance().getUsersFavouritesSet().add(idUser);
    }

    public void deleteFavouriteUser(String idUser){
        Controller.getInstance().getUsersFavouritesSet().remove(idUser);
    }

    //this could be a binary search
    public void insertUserOrdererByName(String userNameToInsert, String nameToSearchPosition){
        LinkedList<String> userLinkedListOrderByName = Controller.getInstance().getUsersListOrderByUserName();
        int i = 0;
        while (i<userLinkedListOrderByName.size() &&
                Controller.getInstance().getUsersHashMap().get(userLinkedListOrderByName.get(i)).getName().getFirst().compareTo(nameToSearchPosition)<0) {
            i++;
        }
        userLinkedListOrderByName.add(i, userNameToInsert);
    }

    public static ArrayList<User> getUsersOrderedByLinkedList(LinkedList<String> usersOrdered, boolean onlyFavourites){
        ArrayList<User> result = new ArrayList<User>();
        for(String usernameKey: usersOrdered){
            if(!onlyFavourites || Controller.getInstance().getUsersFavouritesSet().contains(usernameKey)){
                result.add(Controller.getInstance().getUsersHashMap().get(usernameKey));
            }

        }
        return result;
    }


    public void logUsers(List<User> userList){
        if(userList!=null){
            for(User user: userList){
                Log.i("User",user.toString());
            }
        }
    }

    public static int getUserPosition(String username, ArrayList<User> userList){
        boolean finded=false;
        int result=0;
        int i=0;
        while(!finded && i < userList.size()){
            if(userList.get(i).getLogin().getUsername().equals(username)){
                finded=true;
            }else{
                i++;
            }
        }
        if(finded){
            result=i;
        }
        return result;
    }

    public void callServerGetUsers(final Activity activity){
        UserWebService userService = ServiceGenerator.createService(UserWebService.class, activity,activity.getResources().getString(R.string.loading));
        Call<GetUsersResponse> call = userService.getUsers();
        call.enqueue(new Callback<GetUsersResponse>() {
            @Override
            public void onResponse(Call<GetUsersResponse> call, Response<GetUsersResponse> response) {
                Log.i(activity.getResources().getString(R.string.retrofit_log), activity.getResources().getString(R.string.retrofit_onResponse, WebServicesConstants.CALLSERVERGETUSERS, activity.getClass().getSimpleName()));
                GetUsersResponse usersListResponse = response.body();
                if(usersListResponse!=null && usersListResponse.getResults()!=null){
                    processGetUsersServerCall(usersListResponse);
                    ((MainActivity)activity).showUsers(false);
                    Utils.dismissProgress(activity);
                }else{
                    Log.i(activity.getResources().getString(R.string.retrofit_log), activity.getResources().getString(R.string.retrofit_onFailure, WebServicesConstants.CALLSERVERGETUSERS, activity.getClass().getSimpleName()));
                }
                //((MainActivity)activity).hideSoftKeyboard();
            }

            @Override
            public void onFailure(Call<GetUsersResponse> call, Throwable t) {
                Log.i(activity.getResources().getString(R.string.retrofit_log), activity.getResources().getString(R.string.retrofit_onFailure, WebServicesConstants.CALLSERVERGETUSERS, activity.getClass().getSimpleName()));
                Utils.showInfoDialog(activity.getResources().getString(R.string.retrofit_error, WebServicesConstants.CALLSERVERGETUSERS), activity);
                System.out.println(call);
                //((MainActivity)activity).hideSoftKeyboard();
            }
        });
    }
}
