package com.tuenti.randomco.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Rako on 07/05/2017.
 */
public class UtilsNetwork {

    public static boolean isDeviceOnline(Context aCtx) {
        if(aCtx!=null){
            ConnectivityManager cm =
                    (ConnectivityManager)aCtx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        }else{
            return true;
        }
    }
}
