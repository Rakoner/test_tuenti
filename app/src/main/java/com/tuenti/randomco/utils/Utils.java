package com.tuenti.randomco.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Looper;
import android.util.Log;
import android.widget.TextView;

import com.tuenti.randomco.R;

/**
 * Created by Rako on 05/05/2017.
 */
public class Utils {

    private static ProgressDialog myProgressDialog;

    public static boolean isUIThread() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    public static void showProgressDialog(final String aMessage, Context aCtx) {
        if (aCtx != null) {
            if (myProgressDialog == null) {
                myProgressDialog = new ProgressDialog(aCtx);
                try {
                    if (((Activity) aCtx).isFinishing()) {
                        return;
                    }
                } catch (Exception ex) {
                    Log.e("EXCEPTION", "Exception (showProgressDialog 1): " + ex);
                }
                myProgressDialog.show();
                myProgressDialog.setContentView(R.layout.custom_progressdialog);
                TextView tvProgressDialog=(TextView) myProgressDialog.findViewById(R.id.textViewMessage);
                if(tvProgressDialog!=null){
                    tvProgressDialog.setText(aMessage);
                }
                myProgressDialog.setCancelable(false);
            } else {
                if (aCtx instanceof Activity) {
                    if (Utils.isUIThread()) {
                        if (myProgressDialog != null && myProgressDialog.isShowing()) {
                            ((TextView) myProgressDialog.findViewById(R.id.textViewMessage)).setText(aMessage);
                        }
                    } else {
                        ((Activity) aCtx).runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                if (myProgressDialog != null && myProgressDialog.isShowing()) {
                                    ((TextView) myProgressDialog.findViewById(R.id.textViewMessage)).setText(aMessage);
                                }
                            }
                        });
                    }
                }
            }
        }
    }

    public static void cancelProgressDialog(Context aCtx) {
        try {
            if (aCtx != null) {
                if (Utils.myProgressDialog != null) {
                    try {
                        Utils.myProgressDialog.cancel();
                    } catch (Exception ex) {
                        Log.e("EXCEPTION", "Exception (cancelProgressDialog 1): " + ex);
                    }
                }
                Utils.myProgressDialog = null;
            }
        } catch (Exception e) {
            Log.e("EXCEPTION", "Exception (cancelProgressDialog 2): " , e);
        }
    }

    public static void dismissProgress(Context aCtx){
        cancelProgressDialog(aCtx);
    }

    public static void showInfoDialog(String aMessage, Context ctx) {
        Utils.cancelProgressDialog(ctx);
        if (ctx != null && isUIThread()) {
            new AlertDialog.Builder(ctx).setTitle(ctx.getString(R.string.app_name)).setMessage(aMessage)
                    .setPositiveButton(ctx.getString(R.string.dialog_error_accept), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();
        }
    }

    public static int getScreenOrientation(Context aCtx){
        int orientation = aCtx.getResources().getConfiguration().orientation;
        int result=-1;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            result = Configuration.ORIENTATION_PORTRAIT;
        }
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            result = Configuration.ORIENTATION_LANDSCAPE;
        }
        return orientation;
    }


}
