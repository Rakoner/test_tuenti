package com.tuenti.randomco.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.tuenti.randomco.R;
import com.tuenti.randomco.adapters.UsersAdapter;
import com.tuenti.randomco.beans.user.User;
import com.tuenti.randomco.controller.Controller;
import com.tuenti.randomco.customviews.VerticalSpaceItemDecoration;
import com.tuenti.randomco.services.UserService;
import com.tuenti.randomco.utils.Constants;
import com.tuenti.randomco.utils.Utils;
import com.tuenti.randomco.utils.UtilsNetwork;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by Rako on 05/05/2017.
 */
public class MainActivity extends AppCompatActivity {

    private String screenName=this.getClass().getSimpleName();

    private UsersAdapter usersAdapter;
    private boolean isFavouriteEnabled=false;

    @Bind(R.id.recViewUsers)
    RecyclerView recViewUsers;

    @Bind(R.id.tvNoUser)
    TextView tvNoUser;

    @Bind(R.id.edFinder)
    EditText mEdFinder;

    @Bind(R.id.btnAddUsers)
    Button btnAddUsers;

    @Bind(R.id.btnShowFavourites)
    Button btnShowFavourites;

    @OnTextChanged(R.id.edFinder)
    protected void onFilterFinderChanged() {
        if(usersAdapter != null){
            usersAdapter.getFilter().filter(mEdFinder.getText().toString());
        }
    }

    @OnClick(R.id.btnAddUsers)
    public void addUsers() {
        getUsers();
        mEdFinder.setText("");
    }

    @OnClick(R.id.btnShowFavourites)
    public void showFavourites(View view) {
        if(isFavouriteEnabled){
            showUsers(false);
            btnShowFavourites.setText(getResources().getString(R.string.btn_show_favourites));
        }else{
            showUsers(true);
            btnShowFavourites.setText(getResources().getString(R.string.btn_show_all));
        }
        isFavouriteEnabled=!isFavouriteEnabled;
        mEdFinder.setText("");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initUsersViews();
        addUsers();
    }


    public void initUsersViews(){
        recViewUsers.setHasFixedSize(true);
        recViewUsers.addItemDecoration(new VerticalSpaceItemDecoration(Constants.VERTICAL_ITEM_SPACE));
        recViewUsers.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false));
    }

    public void getUsers(){
        if(UtilsNetwork.isDeviceOnline(MainActivity.this)){
            new UserService().callServerGetUsers(MainActivity.this);
        }else{
            Utils.showInfoDialog(getResources().getString(R.string.internet_no_connection), MainActivity.this);
        }
    }

    public void showUsers(boolean onlyFavourites){
        ArrayList<User> userList = UserService.getUsersOrderedByLinkedList(Controller.getInstance().getUsersListOrderByUserName(),onlyFavourites);
        if(userList.size()>0){
            recViewUsers.setVisibility(View.VISIBLE);
            tvNoUser.setVisibility(View.GONE);
            usersAdapter = new UsersAdapter(userList, MainActivity.this);
            recViewUsers.setAdapter(usersAdapter);
        }else{
            recViewUsers.setVisibility(View.GONE);
            tvNoUser.setVisibility(View.VISIBLE);
        }
    }


}
