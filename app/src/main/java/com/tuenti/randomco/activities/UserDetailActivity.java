package com.tuenti.randomco.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.tuenti.randomco.R;
import com.tuenti.randomco.adapters.UserPagerAdapter;
import com.tuenti.randomco.beans.user.User;
import com.tuenti.randomco.controller.Controller;
import com.tuenti.randomco.services.UserService;
import com.tuenti.randomco.utils.ExtraParams;

import java.util.ArrayList;

/**
 * Created by Rako on 07/05/2017.
 */
public class UserDetailActivity extends AppCompatActivity {

    private ViewPager mViewPager;
    private UserPagerAdapter mUserPagerAdapter;
    private String mUserNameSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);
        Bundle b = getIntent().getExtras();
        mUserNameSelected = b.getString(ExtraParams.EXTRA_USER_USERNAME);
        ArrayList<User> userOrderedList = UserService.getUsersOrderedByLinkedList(Controller.getInstance().getUsersListOrderByUserName(),false);
        mUserPagerAdapter = new UserPagerAdapter(this, userOrderedList);
        int userPosition = UserService.getUserPosition(mUserNameSelected,userOrderedList);
        mViewPager = (ViewPager) findViewById(R.id.pagerUser);
        if(mViewPager!=null && mUserPagerAdapter!=null){
            mViewPager.setAdapter(mUserPagerAdapter);
            mViewPager.setCurrentItem(userPosition);
        }
    }

}
