package com.tuenti.randomco.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.tuenti.randomco.R;
import com.tuenti.randomco.utils.Constants;

/**
 * Created by Rako on 05/05/2017.
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        doSplashStuff();
    }


    public void doSplashStuff() {
        //TODO splash things like initialize libraries or download data
        waitAndGo();
    }

    public void waitAndGo() {
        // Execute some code after 3 seconds have passed
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                goToMainActivity();
            }
        }, Constants.SPLASH_FREEZE_TIME);
    }

    public void goToMainActivity(){
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

}
