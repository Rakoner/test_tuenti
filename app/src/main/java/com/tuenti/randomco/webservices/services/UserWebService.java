package com.tuenti.randomco.webservices.services;

import com.tuenti.randomco.webservices.WebServicesConstants;
import com.tuenti.randomco.webservices.response.GetUsersResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**
 * Created by Rako on 07/05/2017.
 */
public interface UserWebService {

    @Headers("Content-Type: application/json")
    @GET(WebServicesConstants.GET_USERS_SERVICE_URL)
    Call<GetUsersResponse> getUsers();

}
