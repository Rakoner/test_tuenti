package com.tuenti.randomco.webservices;

/**
 * Created by Rako on 07/05/2017.
 */
public class WebServicesConstants {

    public static final String BASE_URL_SERVER="https://api.randomuser.me/";
    public static final String GET_USERS_SERVICE_URL ="?results=40";

    public static final String CALLSERVERGETUSERS="callServerGetUsers";
}
