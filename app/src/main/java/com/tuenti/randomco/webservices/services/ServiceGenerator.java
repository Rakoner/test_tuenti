package com.tuenti.randomco.webservices.services;

import android.content.Context;

import com.tuenti.randomco.utils.Utils;
import com.tuenti.randomco.webservices.WebServicesConstants;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Rako on 07/05/2017.
 */
public class ServiceGenerator {
    private static int TIMEOUT= 60;
    private static Retrofit retrofit=null;

    public static <S> S createService(Class<S> serviceClass, Context aCtx, String aMessage) {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        checkAndShowClock(aCtx, aMessage);
        httpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request request = null;
                try {
                    request = original.newBuilder()
                            .method(original.method(), original.body())
                            .build();
                } catch (Exception e) {
                    //Utils.log("MYRETROFIT", "Exception: ", e);
                }
                // Utils.logObject("MYRETROFIT", "REQUEST", request.body());
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClientBuilder
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(WebServicesConstants.BASE_URL_SERVER)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        return retrofit.create(serviceClass);
    }


    protected static void checkAndShowClock(Context aCtx, String aMessage) {
        if(aMessage!=null && aCtx !=null) {
            Utils.showProgressDialog(aMessage, aCtx);
        }
    }
}
