package com.tuenti.randomco.webservices.response;

import com.tuenti.randomco.beans.user.User;

import java.util.ArrayList;

/**
 * Created by Rako on 07/05/2017.
 */
public class GetUsersResponse {

    private ArrayList<User> results;

    public ArrayList<User> getResults() {
        return results;
    }

    public void setResults(ArrayList<User> results) {
        this.results = results;
    }
}
